Objective: To water gardens, plants based on moisture, sunlight

The project was achieved with the help of:
    1. Arduino Uno
    2. Flow Control
    3. Submersive Water Pump
    4. Light Sensor
    5. Moisture Sensor

Details:

When the moisture level in the soil is low and when the sunlight is high, the motor is turned on automatically and the water is sprayed until the soil's moisture reaches a certain level. Once it reaches a level, the motor will be stopped and the sensor will continue monitoring until the next time it can be started based on the parameters for moisture and sunlight.
